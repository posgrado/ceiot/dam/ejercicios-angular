import { Injectable } from '@angular/core';
import { Dispositivo } from '../model/Dispositivo';

@Injectable({
  providedIn: 'root'
})
export class DispositivoService {
  private _listadoDispositivos: Array<Dispositivo>;
  public get listadoDispositivos(): Array<Dispositivo> {
    return this._listadoDispositivos;
  }

  constructor() {
    this._listadoDispositivos = new Array()
    this._listadoDispositivos.push(new Dispositivo({
      dispositivoId: 1,
      nombre: 'Sensor 1',
      ubicacion: 'Patio',
      electrovalvulaId: 1
    }))
    this._listadoDispositivos.push(new Dispositivo({
      dispositivoId: 2,
      nombre: 'Sensor 2',
      ubicacion: 'Cocina',
      electrovalvulaId: 2
    }))
    this._listadoDispositivos.push(new Dispositivo({
      dispositivoId: 3,
      nombre: 'Sensor 3',
      ubicacion: 'Jardín Delantero',
      electrovalvulaId: 3
    }))
    this._listadoDispositivos.push(new Dispositivo({
      dispositivoId: 4,
      nombre: 'Sensor 4',
      ubicacion: 'Living',
      electrovalvulaId: 4
    }))
    this._listadoDispositivos.push(new Dispositivo({
      dispositivoId: 5,
      nombre: 'Sensor 5',
      ubicacion: 'Patio',
      electrovalvulaId: 5
    }))
  }
}
