interface DispositivoProps {
    dispositivoId: number
    nombre: string
    ubicacion: string
    electrovalvulaId: number
}

export class Dispositivo implements DispositivoProps{
    private _props: DispositivoProps
    
    constructor(props: DispositivoProps){
        this._props = props
    }

    public get dispositivoId(): number {
        return this._props.dispositivoId;
    }
    
    public get nombre(): string {
        return this._props.nombre;
    }
    public set nombre(value: string) {
        this._props.nombre = value;
    }
    
    public get ubicacion(): string {
        return this._props.ubicacion;
    }
    public set ubicacion(value: string) {
        this._props.ubicacion = value;
    }
    
    public get electrovalvulaId(): number {
        return this._props.electrovalvulaId;
    }
    public set electrovalvulaId(value: number) {
        this._props.electrovalvulaId = value;
    }
}